from VideoCapture import Device
import glob
import logging
import Queue
import multiprocessing
import os
import os.path as path
from datetime import datetime, date, timedelta, time
import shutil
from subprocess import Popen, PIPE, STDOUT
import tempfile
import vidcap


class NullRoller(object):
    def save(self, date):
        pass

def _worker(queue):
    while True:
        try:
            (png_dir, movie_dir, pattern) = queue.get(timeout=600)
            MovieMaker.build(png_dir, movie_dir, pattern)
            destination_path = path.join(png_dir, "deleted")
            try:
                os.makedirs(destination_path)
            except WindowsError:
                pass  # Ignore because it probably already exists
            for f in glob.glob(pattern):
                dest = path.join(destination_path, path.basename(f))
                os.rename(f, dest)
        except Queue.Empty:
            time.sleep(60)


class Roller(object):
    def __init__(self, format, png_dir, movie_dir):
        self._format = format
        self._png_dir = png_dir
        self._movie_dir = movie_dir
        self._last_capture = datetime.now()
        self._queue = multiprocessing.Queue()
        self._process = multiprocessing.Process(target=_worker,
                                                args=[self._queue])
        logging.info("Starting Rollover: {0}".format(format))
        self._process.start()

    def save(self, date_):
        if self._mask(date_) != self._mask(self._last_capture):
            logging.info("Building Date Video: {0}".format(self._mask(date_)))

            self._queue.put((
                self._png_dir,
                self._movie_dir,
                MovieMaker.make_glob(self._last_capture, self._format)
            ))
            self._last_capture = date_

    def _mask(self, date_):
        return date_.strftime(self._format)

class _DeviceManager(object):
    def __init__(self):
        self._device = None
        self._count = 0

    def enter(self):
        if self._device is None:
            self._device = Device()
        self._count += 1

    def exit(self):
        if self._count % 5 == 0:
            self._count = 0
            if self._device is not None:
                del self._device
                self._device = None

    def capture(self, filename):
        self._device.saveSnapshot(filename)



class DailyCapture(object):
    def __init__(self, outdir, save_camera=False, roller=NullRoller()):
        self._device = _DeviceManager()
        self._output_directory = outdir
        self._roller = roller

    def __enter__(self):
        try:
            self._device.enter()
        except Exception, ex:
            logging.error("Unable to create capture device: {0}".format(ex))
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self._device is not None:
            try:
                date = self.save()
            except Exception, ex:
                pass

        self._device.exit()
        self._roller.save(date)

    def save(self):
        date = datetime.now()
        filename = path.join(
            self._output_directory,
            date.strftime("%Y%m%d%H%M%S.png")
        )
        self._device.capture(filename)
        logging.info("Captured {0}".format(filename))
        return date

class MovieMaker(object):
    FFMPEG = path.abspath(path.join(path.dirname(__file__), "ffmpeg.exe"))
    OPTIONS = [ "-y", "-f", "image2", "-r", "24", '-vcodec', 'png', '-i', ]
    BETWEEN = [ "-pix_fmt", "yuv420p" ]

    def __init__(self, png_dir, movie_dir):
        self._png_dir = png_dir
        self._movie_dir = movie_dir

    @classmethod
    def build(cls, png_dir, movie_dir, pattern):
        filename = pattern.replace('*', '')

        outfile = path.abspath(path.join(movie_dir, filename))
        in_files = glob.glob(path.abspath(path.join(png_dir,
                                                "{0}.png".format(pattern))))

        cls.save(in_files, outfile)

    @classmethod
    def save(cls, files, movie):
        tmpdir = cls._renumber_files(files)
        cmd = [ cls.FFMPEG ]
        cmd.extend(cls.OPTIONS)
        cmd.append("c%010d.png")
        cmd.extend(cls.BETWEEN)
        cmd.append("{0}.mp4".format(movie))
        logging.debug("ffmpeg options: {0}".format(cmd))

        pipe = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=STDOUT,
                     universal_newlines=True, cwd=tmpdir)
        (stdout, _) = pipe.communicate()
        logging.debug("FFMPEG OUTPUT: {0}".format(stdout))
        shutil.rmtree(tmpdir, lambda x, y, z: logging.warn("Failed to remove: {0}".format(y)))

    @classmethod
    def _renumber_files(cls, files):
        count = 0
        tmpdir = tempfile.mkdtemp()
        for f in files:
            dest = path.abspath(path.join(tmpdir, "c{0:010d}.png".format(count)))
            count += 1
            shutil.copyfile(f, dest)

        return tmpdir

    @classmethod
    def make_glob(cls, date, format):
        return date.strftime("{0}*".format(format))
