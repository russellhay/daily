""" A small command line tool for taking a picture with a webcam. """
import glob

import dailycapture
import time
import argparse
import os
import os.path as path
from datetime import date, timedelta
import logging

_ROLLOVER = {
    'minute': '%Y%m%d%H%M',
    'hour': '%Y%m%d%H',
    'day': "%Y%m%d",
    'month': "%Y%m",
}

def take_snapshot(output_directory, movie_directory, periodic, rollover=None):
    if rollover is not None:
        rollover = dailycapture.Roller(
            format=_ROLLOVER[rollover],
            png_dir=output_directory,
            movie_dir=movie_directory
        )
    else:
        rollover = dailycapture.NullRoller()

    capture = dailycapture.DailyCapture(output_directory, True, rollover)
    with capture:
        pass # Strange Idiom, but a picture is taken here
    while periodic > 0:
        time.sleep(periodic)
        with capture:
            pass # Strange Idiom, but a picture is taken here


def build_movie(png_directory, pattern, movie_directory):
    logging.info("Creating with pattern: {0}".format(pattern))
    dailycapture.MovieMaker.build(png_directory, movie_directory, pattern)


def _silent_makedirs(dirname):
    try:
        os.makedirs(dirname)
    except WindowsError:
        pass  # Ignore failed attempts


def _make_last_month_glob():
    today = date.today()
    year = today.year
    month = today.month
    if month == 1:
        year -= 1
        month = 12
    else:
        month -= 1
    yesterday = today.replace(year=year, month=month)
    return dailycapture.MovieMaker.make_glob(yesterday, "%Y%m")

def _make_day_glob(offset):
    yesterday = date.today() + timedelta(days=offset)
    return dailycapture.MovieMaker.make_glob(yesterday, "%Y%m%d")

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    parser = argparse.ArgumentParser(description=__doc__)

    build_group = parser.add_mutually_exclusive_group(required=False)
    build_group.add_argument("--glob", default=None,
                        help="builds a timelapse movie based on a glob pattern")
    build_group.add_argument("--month", required=False, default=False,
                        action="store_const", const=True,
                        help="builds a timelapse movie for last month")
    build_group.add_argument("--yesterday", required=False, default=False,
                             action="store_const", const=True,
                             help="builds a timelapse movie for yesterday")
    build_group.add_argument("--today", required=False, default=False,
                             action="store_const", const=True,
                             help="builds a timelapse movie for today")
    parser.add_argument("--out", required=False,
                        default=path.abspath(path.join(path.dirname(__file__),
                                                       "output")))
    parser.add_argument("--movie", required=False,
                        default=path.abspath(path.join(path.dirname(__file__),
                                                                    "movie")))
    parser.add_argument("--periodic", required=False, type=int, default=30,
                        help="take pictures periodically (seconds)")
    parser.add_argument("--rollover", required=False, choices=_ROLLOVER.keys(),
                        help="specify when to roll over to a new video file")

    args = parser.parse_args()
    print args
    _silent_makedirs(args.out)
    _silent_makedirs(args.movie)

    if args.glob is not None:
        build_movie(args.out, args.glob, args.movie)
    elif args.month:
        pattern = _make_last_month_glob()
        build_movie(args.out, pattern, args.movie)
    elif args.yesterday:
        pattern = _make_day_glob(-1)
        build_movie(args.out, pattern, args.movie)
    elif args.today:
        pattern = _make_day_glob(0)
        build_movie(args.out, pattern, args.movie)
    else:
        take_snapshot(args.out, args.movie, args.periodic, args.rollover)
